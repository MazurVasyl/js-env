export class IncorrectValue extends Error {
    constructor(message) {
        super();
        this.name = 'IncorrectValue';
        this.message = message;
    }
}

export default class Control {
    static isString(value, valueName) {
        if (typeof (value) !== 'string') {
            throw new IncorrectValue(
                `'${valueName}' parameter should be a string, but ${value} is '${typeof (value)}' type`
            );
        }
    }

    static emptyString(value, valueName) {
        if (value.length !== 0) {
            throw new IncorrectValue(`'${valueName}' parameter should be an empty string`);
        }
    }

    static validName(value, valueName) {
        if (value.split('').slice(1).filter(val => val.codePointAt(0) < 97 || val.codePointAt(0) > 122).length > 0) {
            throw new IncorrectValue(
                `'${valueName}' parameter starts only with uppercase, other letters should be in lowercase`
            );
        }
    }

    static startUppercase(value, valueName) {
        if (value.codePointAt(0) < 65 || value.codePointAt(0) > 90) {
            throw new IncorrectValue(`'${valueName}' parameter should start with uppercase`);
        }
    }

    static emptyArray(value, valueName) {
        if (!Array.isArray(value) || value.length !== 0) {
            throw new IncorrectValue(`'${valueName}' parameter should be an empty array: '[]'`);
        }
    }

    static taskStatusOpen(value, valueName) {
        if (value !== 'open') {
            throw new IncorrectValue(`'${valueName}' parameter should be called 'open'`);
        }
    }

    static isNumber(value, valueName) {
        if (typeof (value) !== 'number' || isNaN(value)) {
            throw new IncorrectValue(`'${valueName}' parameter should be a 'number'`);
        }
    }

    static isDate(value, valueName) {
        if (!(new Date(value))) {
            throw new IncorrectValue(`'${valueName}' parameter should be 'new Date()'`);
        }
    }

    static validateClass(value, cls, paramName) {
        if (!(value instanceof cls)) {
            let valueCls;
            try {
                valueCls = `instance of ${value.__proto__.constructor.name}`;
            } catch {
                valueCls = 'something strange';
            }

            throw new IncorrectValue(
                `'${paramName}' parameter should be instance of class '${cls.name}', but ${valueCls} received`
            );
        }
    }

    static taskStatus(value, valueName) {
        if (value !== 'open' && value !== 'in dev' && value !== 'fixed') {
            throw new IncorrectValue(`'${valueName}' parameter should be 'open' or 'in dev' or 'fixed'`);
        }
    }
}

