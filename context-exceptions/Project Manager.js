import Control from "./control.js";
import Release from "./Release.js";

export default class ProjectManager {
    constructor(firstName, lastName, assignedReleases) {
        Control.isString(firstName, 'firstName');
        Control.startUppercase(firstName, 'firstName');
        Control.validName(firstName, 'firstName');

        Control.isString(lastName, 'lastName');
        Control.startUppercase(lastName, 'lastName');
        Control.validName(lastName, 'lastName');

        Control.emptyArray(assignedReleases, 'assignedReleases');

        this.firstName = firstName;
        this.lastName = lastName;
        this.assignedReleases = assignedReleases;
    }

    set assign(release) {
        Control.validateClass(release, Release, 'release');

        if (!this.assignedReleases.find(value => value.name === release.name)) {
            this.assignedReleases.push(release);
        }
        release.assign = this;
    }
}
