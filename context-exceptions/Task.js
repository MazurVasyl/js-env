import Control from "./control.js";
import Developer from "./Developer.js";

export default class Task {
    constructor(name, description, status, estimate, loggedWork, assignee) {
        Control.isString(name, 'name');
        Control.isString(description, 'description');

        Control.isString(status, 'status');
        Control.taskStatusOpen(status, 'status');

        Control.isNumber(estimate, 'estimate');
        Control.isNumber(loggedWork, 'loggedWork');

        Control.isString(assignee, 'assignee');
        Control.emptyString(assignee, 'assignee');

        this.ID = Task.f();
        this.name = name;
        this.description = description;
        this.status = status;
        this.estimate = estimate;
        this.loggedWork = loggedWork;
        this.assignee = assignee;
    }

    set assign(dev) {
        Control.validateClass(dev, Developer, 'dev');

        if (!dev.assignedTasks.find(value => value.name === this.name)) {
            dev.assignedTasks.push(this);
        }
        this.assignee = dev;
    }

    set Status(s) {
        Control.taskStatus(s, 's');
        this.status = s;
    }

    set Estimate(t) {
        Control.isNumber(t, 't');
        this.estimate = t;
    }

    set LoggedWork(t) {
        Control.isNumber(t, 't');
        this.loggedWork += t;
    }

    static get RISK_FACTOR() {
        return 1.3;
    }

    get restOfTime() {
        return Task.RISK_FACTOR * this.estimate - this.loggedWork;
    }
}

const counter = (x = 0) => () => ++x;
Task.f = counter();
