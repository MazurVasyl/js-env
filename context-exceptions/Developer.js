import Control from "./control.js";
import Task from "./Task.js";

export default class Developer {
    constructor(firstName, lastName, assignedTasks) {
        Control.isString(firstName, 'firstName');
        Control.startUppercase(firstName, 'firstName');
        Control.validName(firstName, 'firstName');

        Control.isString(lastName, 'lastName');
        Control.startUppercase(lastName, 'lastName');
        Control.validName(lastName, 'lastName');

        Control.emptyArray(assignedTasks, 'assignedTasks');

        this.firstName = firstName;
        this.lastName = lastName;
        this.assignedTasks = assignedTasks;
    }

    set assign(task) {
        Control.validateClass(task, Task, 'task');

        if (!this.assignedTasks.find(value => value.name === task.name)) {
            this.assignedTasks.push(task);
        }
        task.assign = this;
    }

    get fullName() {
        return `${this.firstName} ${this.lastName}`;
    }
}
