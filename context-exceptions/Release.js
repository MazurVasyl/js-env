import Control from "./control.js";
import ProjectManager from "./Project Manager.js";
import Task from "./Task.js";

export default class Release {
    constructor(name, description, releaseDate, tasks){
        Control.isString(name, 'name');
        Control.isString(description, 'description');
        Control.isDate(releaseDate, 'releaseDate');
        Control.emptyArray(tasks, 'tasks');

        this.name = name;
        this.description = description;
        this.releaseDate = releaseDate;
        this.tasks = tasks;
    }

    set assign(pM) {
        Control.validateClass(pM, ProjectManager, 'pM');

        if (!pM.assignedReleases.find(value => value.name === this.name)) {
            pM.assignedReleases.push(this);
        }
        this.projectManager = pM;
    }

    set addTask(task) {
        Control.validateClass(task, Task, 'task');
        this.tasks.push(task);
    }

    set newReleaseDate(date) {
        Control.isDate(date, 'date');
        this.releaseDate = date;

        if (this.releaseDate.getDay() === 0) {
            this.releaseDate.setDate(this.releaseDate.getDate() + 1);
        }
        if (this.releaseDate.getDay() === 6) {
            this.releaseDate.setDate(this.releaseDate.getDate() + 2);
        }

        return this.releaseDate;
    }

    get devsRestOfTime() {
        return this.tasks.reduce((devs, task) => {
            (!Object.keys(devs).find(value => value === task.assignee.fullName))
                ? devs[task.assignee.fullName] = task.restOfTime
                : devs[task.assignee.fullName] += task.restOfTime;

            return devs;
        }, {});
    }

    get maxRestOfTime() {
        return Math.max(...Object.values(this.devsRestOfTime));
    }

    static get WORK_TIME() {
        return 330;
    }

    get finishDay() {
        const days = Math.trunc(this.maxRestOfTime / Release.WORK_TIME);
        const now = new Date();

        now.setDate(now.getDate() +  Math.trunc(days / 5) * 7 + days % 5);
        now.setMinutes(now.getMinutes() + this.maxRestOfTime % Release.WORK_TIME);
        now.setDate(now.getDate() + 7);

        return now;
    }

    static control(release) {
        Control.validateClass(release, Release, 'release');

        (release.finishDay.toISOString() <= release.releaseDate.toISOString())
            ? console.log(`alright, we have enough time to the releaseDate in ${release.name}`)
            : console.log(`we have to move the releaseDate in ${release.name} for 
                ${Math.ceil((release.finishDay - release.releaseDate) / 86400000)} days`);
    }
}
