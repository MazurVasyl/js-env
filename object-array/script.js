// We have to find the percentage of male and female are working in IT in each city

function collection(arrayOfData) {

    const ourCollection = arrayOfData.map(item => {
        const [, cityName,,,,,,,,,gender] = item;

        return (gender === "мужской") ? {city: cityName, male: 1, female: 0} : {city: cityName, male: 0, female: 1};
    });

    const result = ourCollection.reduce((allCity, currentObject) => {
        let city = allCity.find(obj => obj.city === currentObject.city);

        if (!city) {
            city = {city: currentObject.city, male: 0, female: 0};
            allCity.push(city);
        }

        city.male += currentObject.male;
        city.female += currentObject.female;
        return allCity;
    }
    ,[]);
    console.log(result);

    return result.map(value => {
        const valueMale = value.male;
        return {cityName: value.city,
                malePercent: Math.round(value.male / (value.male + value.female) * 100),
                femalePercent: Math.round(value.female / (valueMale + value.female) * 100)};
    });
}

const percentages = collection(data);
console.log(percentages);

// creating svg and 100% Stacked Bar Chart
const createElement = (qualifierName, parentNode) => {
    const element = document.createElementNS('http://www.w3.org/2000/svg', qualifierName);
    parentNode.appendChild(element);
    return element;
};

const setAttributes = (node, ...attributes) => {
    attributes.forEach(item => {
        Object.entries(item).forEach(([key, value]) => {
            node.setAttribute(key, value);
        });
    });
};

const lineStyle = {style: 'stroke: darkslateblue; stroke-width: 1;'};
const textNetStyle = {fill: 'darkslateblue', style: 'font-size: 20px;'};
const textBlockStyle = {fill: 'darkslateblue'};
const textPercent = {fill: 'white'};

const svg = createElement('svg', document.body);
setAttributes(svg, {
    width: 1500,
    height: 700,
    style: 'background: cornsilk;',
});

const headline = createElement('text', svg);
setAttributes(headline, {
    x: 400,
    y: 25,
    fill: 'black',
    style: 'font-size: 25px;',
});
headline.textContent = 'The percentage of male and female are working in IT in each city';

const netting = createElement('g', svg);

const lineY = createElement('line', netting);
setAttributes(lineY, {
    x1: 50,
    y1: 50,
    x2: 50,
    y2: 550,
}, lineStyle);

for (let i = 0; i < 6; i++) {

    const line = createElement('line', netting);
    setAttributes(line, {
        x1: 50,
        y1: 50 + i * 100,
        x2: 1450,
        y2: 50 + i * 100,
    }, lineStyle);
}

for (let i = 0; i < 4; i++) {

    const text = createElement('text', netting);
    setAttributes(text, {
        x: 25,
        y: 455 - i * 100,
    }, textNetStyle);
    text.textContent = 20 + i * 20;
}

const text100 = createElement('text', netting);
setAttributes(text100, {
    x: 15,
    y: 55,
}, textNetStyle);
text100.textContent = 100;

const inform = createElement('g', svg);

const infoMale = createElement('rect', inform);
setAttributes(infoMale, {
    fill: 'darkblue',
    x: 1462,
    y: 310,
    width: 25,
    height: 25,
});

const male = createElement('text', inform);
setAttributes(male, {
    x: 1470,
    y: 350,
});
male.textContent = 'm';

const infoFemale = createElement('rect', inform);
setAttributes(infoFemale, {
    fill: '#ff3616',
    x: 1462,
    y: 250,
    width: 25,
    height: 25,
});

const female = createElement('text', inform);
setAttributes(female, {
    x: 1470,
    y: 290,
});
female.textContent = 'f';

percentages.forEach((city, index) => {

    const heightMale = 5 * city.malePercent;
    const heightFemale = 5 * city.femalePercent;
    const stepBlockX = 75 + index * 55;
    const stepTextX = 81 + index * 55;

    const cityBlock = createElement('g', svg);

    const rectFemale = createElement('rect', cityBlock);
    setAttributes(rectFemale, {
        fill: '#ff3616',
        x: stepBlockX,
        y: 50,
        width: 30,
        height: heightFemale,
    });

    const textFemale = createElement('text', cityBlock);
    setAttributes(textFemale, {
        x: stepTextX,
        y: 56 + heightFemale / 2,
    }, textPercent);
    textFemale.textContent = city.femalePercent;

    const rectMale = createElement('rect', cityBlock);
    setAttributes(rectMale, {
        fill: 'darkblue',
        x: stepBlockX,
        y: 50 + heightFemale,
        width: 30,
        height: heightMale,
    });

    const textMale = createElement('text', cityBlock);
    setAttributes(textMale, {
        x: stepTextX,
        y: 56 + heightFemale + heightMale / 2,
    }, textPercent);
    textMale.textContent = city.malePercent;

    const cityName = createElement('text', cityBlock);
    setAttributes(cityName, {
        x: stepBlockX,
        y: 600,
        transform: `rotate(-90 ${125 + index * 55},630)`,
    }, textBlockStyle);
    cityName.textContent = city.cityName;
});
