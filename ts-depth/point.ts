import {IPoint} from "./interface";

export default class Point implements IPoint{
    x: number;
    y: number;

    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }

    static randomNumber(from: number, to: number): number{
        return Math.random() * (to - from) + from;
    }

    static randomPoint(xFrom: number, xTo: number, yFrom: number, yTo: number): Point {
        return new Point(
            Point.randomNumber(xFrom, xTo),
            Point.randomNumber(yFrom, yTo)
        );
    }
}
