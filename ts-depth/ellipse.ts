import {IPoint, IEllipse} from "./interface";

export default class Ellipse implements IEllipse{
    center: IPoint;
    radius1: number;
    radius2: number;

    constructor(center: IPoint, radius1: number, radius2: number) {
        this.center = center;
        this.radius1 = radius1;
        this.radius2 = radius2;
    }

    isCircle(): boolean {
        return this.radius1 === this.radius2;
    }

    contains(point: IPoint): boolean {
        return (point.x - this.center.x) ** 2 / this.radius1 ** 2 + (point.y - this.center.y) ** 2 / this.radius2 ** 2 < 1;
    }

    area(): number {
        return Math.PI * this.radius1 * this.radius2;
    }
}
