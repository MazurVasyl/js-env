import {IField, IPoint} from "./interface";
import Rectangle from "./rectangle";

export default class Field implements IField{
    ctx: CanvasRenderingContext2D;

    static WIDTH(): number {
        return 800;
    }

    static HEIGHT(): number {
        return 500;
    }

    constructor(ctx: CanvasRenderingContext2D) {
        this.ctx = ctx;
    }

    strokeRectangle(rectangle: Rectangle): void {
        this.ctx.strokeStyle = '#595959';
        this.ctx.lineWidth = 2;
        this.ctx.strokeRect(rectangle.left(), rectangle.top(), rectangle.width(), -rectangle.height());
    }

    fillDot(point: IPoint): void {
        this.ctx.beginPath();
        this.ctx.arc(point.x, point.y, 4, 0, 2 * Math.PI);
        this.ctx.fillStyle = 'red';
        this.ctx.fill();
    }

    strokeEllipse(rectangle: Rectangle): void {
        const x = rectangle.left() + rectangle.width() / 2;
        const y = rectangle.bottom() + rectangle.height() / 2;

        this.ctx.beginPath();
        this.ctx.ellipse(x, y, rectangle.width() / 2, rectangle.height() / 2, Math.PI, 0, 2 * Math.PI);
        this.ctx.stroke();
    }
}
