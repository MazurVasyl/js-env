import Point from './point';
import Line from './line';
import {IPoint, IRectangle} from "./interface";

export default class Rectangle implements IRectangle{
    point1: IPoint;
    point2: IPoint;

    constructor(point1: IPoint, point2: IPoint) {
        this.point1 = point1;
        this.point2 = point2;
    }

    isSquare(): boolean {
        const lb: Point = new Point(this.left(), this.bottom());
        const lt: Point = new Point(this.left(), this.top());
        const rb: Point = new Point(this.right(), this.bottom());
        return new Line(lb, lt).length() === new Line(lb, rb).length();
    }

    contains(point: IPoint): boolean {
        return point.x > this.left() &&
            point.x < this.right() &&
            point.y > this.bottom() &&
            point.y < this.top();
    }

    left(): number {
        return Math.min(this.point1.x, this.point2.x);
    }

    right(): number {
        return Math.max(this.point1.x, this.point2.x);
    }

    bottom(): number {
        return Math.min(this.point1.y, this.point2.y);
    }

    top(): number {
        return Math.max(this.point1.y, this.point2.y);
    }

    width(): number {
        return this.right() - this.left();
    }

    height(): number {
        return this.top() - this.bottom();
    }

    area(): number {
        return this.width() * this.height();
    }

    intersect(rectangle: Rectangle): Rectangle | undefined {
        const
            left: number = Math.max(rectangle.left(), this.left()),
            right: number = Math.min(rectangle.right(), this.right()),
            top: number = Math.min(rectangle.top(), this.top()),
            bottom: number = Math.max(rectangle.bottom(), this.bottom());

        if (right > left && top > bottom) {
            return new Rectangle(new Point(left, bottom), new Point(right, top));
        }
    }
}
