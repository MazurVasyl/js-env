import {IPoint, ILine} from "./interface";

export default class Line implements ILine{
    start: IPoint;
    end: IPoint;

    constructor(start: IPoint, end: IPoint) {
        this.start = start;
        this.end = end;
    }

    length(): number {
        return Math.sqrt(Math.pow(this.start.x - this.end.x, 2) + Math.pow(this.start.y - this.end.y, 2));
    }
}
