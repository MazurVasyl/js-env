import Rectangle from "./rectangle";

export interface IPoint {
    x: number;
    y: number;
}

export interface ILine {
    start: IPoint;
    end: IPoint;
    length(): number;
}

export interface IRectangle {
    point1: IPoint;
    point2: IPoint;
    isSquare(): boolean;
    contains(point: IPoint): boolean;
    left(): number;
    right(): number;
    top(): number;
    bottom(): number;
    width(): number;
    height(): number;
    area(): number;
    intersect(rectangle: Rectangle): Rectangle | undefined
}

export interface IEllipse {
    center: IPoint;
    radius1: number;
    radius2: number;
    isCircle(): boolean;
    contains(point: IPoint): boolean;
    area(): number;
}

export interface IField {
    ctx: CanvasRenderingContext2D;
    strokeRectangle(rectangle: Rectangle): void;
    fillDot(point: IPoint): void;
    strokeEllipse(rectangle: Rectangle): void;
}

export interface HTMLCanvasElement {
    captureStream()
}
