// European countries:(38)  Population:         Area:                EU:(26)  EZ:(18)  SA:(24)
// Albania                  2,887,000           28,748 km2,             N       N       N
// Austria                  8,857,960           83,871 km2,             Y       Y       Y
// Belarus                  9,491,800	        207,600 km2,            N       N       N
// Belgium                  11,502,204	        30,528 km2,             Y       Y       Y
// Bosnia and Herzegovina   3,511,372	        51,197 km2,             N       N       N
// Bulgaria                 7,050,034	        110,879 km2,            Y       N       N
// Croatia                  4,105,493	        56,594 km2,             Y       N       N
// Cyprus                   1,172,458	        9,251 km2,              Y       Y       N
// Czech Republic           10,625,449	        78,867 km2,             Y       N       Y
// Denmark                  5,806,015	        42,933 km2,             Y       N       Y
// Estonia                  1,315,000	        45,228 km2,             Y       Y       Y
// Finland                  5,519,463	        338,145 km2,            Y       Y       Y
// France                   67,348,000	        643,427 km2,            Y       Y       Y
// Germany                  82,800,000	        357,022 km2,            Y       Y       Y
// Greece                   10,816,286	        131,957 km2,            Y       Y       Y
// Hungary                  9,771,000	        93,028 km2,             Y       N       Y
// Iceland                  350,710             103,000 km2,            N       N       Y
// Ireland                  4,857,000	        70,273 km2,             Y       Y       N
// Italy                    60,494,118	        301,340 km2,            Y       Y       Y
// Latvia                   1,934,379	        64,589 km2,             Y       Y       Y
// Lithuania                2,797,000	        65,300 km2,             Y       Y       Y
// Luxembourg               602,000             2,586 km2,              Y       Y       Y
// Moldova                  3,350,900	        33,851 km2,             N       N       N
// Montenegro               642,550             13,812 km2,             N       N       N
// Netherlands              17,272,990	        41,543 km2,             Y       Y       Y
// North Macedonia          2,075,301	        25,713 km2,             N       N       N
// Norway                   5,323,933	        323,802 km2,            N       N       Y
// Poland                   38,433,600	        312,685 km2,            Y       N       Y
// Portugal                 10,291,196	        92,090 km2,             Y       Y       Y
// Romania                  19,622,000	        238,391 km2,            Y       N       N
// Serbia                   7,001,444	        88,361 km2,             N       N       N
// Slovakia                 5,445,087	        49,035 km2,             Y       Y       Y
// Slovenia                 2,070,050	        20,273 km2,             Y       Y       Y
// Spain                    47,720,291	        505,370 km2,            Y       Y       Y
// Sweden                   10,221,988	        450,295 km2,            Y       N       Y
// Switzerland              8,544,034	        41,277 km2,             N       N       Y
// Ukraine                  44,291,413	        603,550 km2,            N       N       N
// United Kingdom           66,040,229	        243,610 km2,            N       N       N


const countriesName = ['Albania', 'Austria', 'Belarus', 'Belgium', 'Bosnia and Herzegovina',
    'Bulgaria', 'Croatia', 'Cyprus', 'Czech Republic', 'Denmark', 'Estonia', 'Finland',
    'France', 'Germany', 'Greece', 'Hungary', 'Iceland', 'Ireland', 'Italy', 'Latvia',
    'Lithuania', 'Luxembourg', 'Moldova', 'Montenegro', 'Netherlands', 'North Macedonia',
    'Norway', 'Poland', 'Portugal', 'Romania', 'Serbia', 'Slovakia', 'Slovenia', 'Spain',
    'Sweden', 'Switzerland', 'Ukraine', 'United Kingdom'];

console.log('Countries names:');
console.log(countriesName);
console.log('------------------------------');

// if a country is in EU or EZ or SA  =1;  else =0.

let allCountriesInfo = [
    [2887000, 28748, 0, 0, 0],
    [8857960, 83871, 1, 1, 1],
    [9491800, 207600, 0, 0, 0],
    [11502204, 30528, 1, 1, 1],
    [3511372, 51197, 0, 0, 0],
    [7050034, 110879, 1, 0, 0],
    [4105493, 56594, 1, 0, 0],
    [1172458, 9251, 1, 1, 0],
    [10625449, 78867, 1, 0, 1],
    [5806015, 42933, 1, 0, 1],
    [1315000, 45228, 1, 1, 1],
    [5519463, 338145, 1, 1, 1],
    [67348000, 643427, 1, 1, 1],
    [82800000, 357022, 1, 1, 1],
    [10816286, 131957, 1, 1, 1],
    [9771000, 93028, 1, 0, 1],
    [350710, 103000, 0, 0, 1],
    [4857000, 70273, 1, 1, 0],
    [60494118, 301340, 1, 1, 1],
    [1934379, 64589, 1, 1, 1],
    [2797000, 65300, 1, 1, 1],
    [602000, 2586, 1, 1, 1],
    [3350900, 33851, 0, 0, 0],
    [642550, 13812, 0, 0, 0],
    [17272990, 41543, 1, 1, 1],
    [2075301, 25713, 0, 0, 0],
    [5323933, 323802, 0, 0, 1],
    [38433600, 312685, 1, 0, 1],
    [10291196, 92090, 1, 1, 1],
    [19622000, 238391, 1, 0, 0],
    [7001444, 88361, 0, 0, 0],
    [5445087, 49035, 1, 1, 1],
    [2070050, 20273, 1, 1, 1],
    [47720291, 505370, 1, 1, 1],
    [10221988, 450295, 1, 0, 1],
    [8544034, 41277, 0, 0, 1],
    [44291413, 603550, 0, 0, 0],
    [66040229, 243610, 0, 0, 0],
];

// make numbers more convenient to work with them: 'population' and 'area'  /1000

for (let i = 0; i < allCountriesInfo.length; i++) {

    for (let j = 0; j < 2; j++) {
        allCountriesInfo[i][j] = Math.round(allCountriesInfo[i][j] / 1000);
    }
}

console.log('All Countries Information:');
console.log(allCountriesInfo);
console.log('------------------------------');

// we look for how many bits we need to shift left
// for it we find the biggest area (allCountriesInfo[i][1]) and see its amount of bits

let areas = [];
for (let i = 0; i < allCountriesInfo.length; i++) {

    areas.push(allCountriesInfo[i][1]);
}

console.log('Areas of countries:');
console.log(areas);
console.log('------------------------------');

let maxArea = Math.max(... areas);
console.log(`max area: ${maxArea}`);

// amount of bits in maxArea
let bitsNum = 0;

while (maxArea >= 1) {
    bitsNum++;
    maxArea = maxArea >> 1;
}

console.log(`Amount of bits in max area: ${bitsNum}`);              // its 10
console.log('------------------------------');

function mask(num) {                         // num - how many bits we have to check

    let result = 0;

    for (let i = 0; i < num; i++){
        result = result + Math.pow(2, i);
    }

    return result;
}


// shift to the left by 10 bits each number of population
// then add  numbers of area with bitwise OR operator
// then shift to the left by one bit for EU, EZ, SA and add them  with bitwise OR operator
// put result in new array "numbers"

let numbers = [];

for (let i = 0; i < allCountriesInfo.length; i++) {
    let countryNumber = allCountriesInfo[i][0];

    countryNumber = countryNumber << bitsNum;
    countryNumber = countryNumber | allCountriesInfo[i][1];

    for (let j = 2; j < 5; j++) {
        countryNumber = countryNumber << 1;
        countryNumber = countryNumber | allCountriesInfo[i][j];
    }

    numbers[i] = countryNumber;
}

console.log('numbers of countries:');
console.log(numbers);
console.log('----------------------------------');

// functions
// funA - A. countries that are members of the Eurozone and are not members of the European Union
// funB - B. countries, with population of over 20 million people, with an area of less than 200
//           thousand km2, are members of the Schengen area and are not members of the Eurozone
// funC - C. countries with a population density of more than 200 and less than 400 people per km2


// funA
function funA(arr) {                    //    EU  - bit number 3, have to be = 0
                                        //    EZ  - bit number 2, have to be = 1

    let groupA = arr.filter(el => (el >> 1 & mask(2)) === 1);           // === 1, because result mast be 01 (it's 1)

    return groupA;
}


// funB
function funB(arr) {                    //   EU, EZ, SA - 3 bits,  for area - 10 bits  (all together = 13 bits)
                                        //   it means information about areas begins from 4-s bit
    let groupB = [];                    //   and about population begins from 14-s bit

    for (let i = 0; i < arr.length; i++) {

        if (arr[i] >> 13 > 20000 && (arr[i] >> 3 & mask(10)) <= 200 && (arr[i] & mask(2)) === 1) {
            groupB.push(arr[i]);
        }
    }
    return groupB;
}


// funC
function funC(arr) {                    //   EU, EZ, SA - 3 bits,  for area - 10 bits  (all together = 13 bits)
                                        //   it means information about areas begins from 4-s bit
    let groupC = [];                    //   and about population begins from 14-s bit (index = 13)

    for (let i = 0; i < arr.length; i++) {

        let popul = arr[i] >> 13;
        let area = arr[i] >> 3 & mask(10);
        let densityPopul = popul / area;

        if (densityPopul >= 200 && densityPopul <= 400) {

            groupC.push(arr[i]);
        }
    }
    return groupC;
}


console.log('are in EZ but are not in EU:');
let resultFunA = funA(numbers);
console.log(resultFunA);
convertFun(resultFunA);
console.log('---------------------------');

console.log('more than 20 m people, area is less than 200000 km2, are in SA and not are in EZ :');
let resultFunB = funB(numbers);
console.log(resultFunB);
convertFun(resultFunB);
console.log('---------------------------');

console.log('density of population more than 200 and less than 400:');
let resultFunC = funC(numbers);
console.log(resultFunC);
convertFun(resultFunC);

// convertation numbers to names
function convertFun(arr) {
    let convertFun = [];
    for (let i = 0; i < arr.length; i++) {

        let position = numbers.indexOf(arr[i]);

        convertFun.push(countriesName[position]);
    }
    console.log(convertFun);
}

console.log('---------------------------');


// density of population (just to check)
// console.log('----------------density--------------');
// for (let i = 0; i < allCountriesInfo.length; i++) {
//     console.log(allCountriesInfo[i][0] / allCountriesInfo[i][1]);
// }
// console.log('------------------------------');
//
//  function funA1(arr) {
//
//  //    EU  - bit with index 2
//  //    EZ  - bit with index 1
//
//
//      let groupA = arr.filter(el => el >> 1 & mask(2) === 3);
//      // for (let i = 0; i < arr.length; i++) {
//      //
//      //     if ((arr[i] & mask) === 6) {
//      //         groupA.push(arr[i]);
//      //     }
//      // }
//
//      return groupA;
//  }
//
//  console.log('are in EZ and are in EU:');
//  let resultFunA1 = funA1(numbers);
//  console.log(resultFunA1);
//  convertFun(resultFunA1);
//
