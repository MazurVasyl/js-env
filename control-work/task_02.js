/*
    Дано граф вузли якого представлені обєктами
        Node = {
            value: number,
            children: Node[]
        }
    Порахувати кількість листків - нод без дітей.
 */

const {graph} = require('./graph');

const leafs = graph => {
    // TODO: Place your solution here!

    if (!graph.children) {

        return 1;
    }
    else{
        return graph.children.reduce((allLeafs, curValue) => {

            curValue = leafs(curValue);

            return allLeafs + curValue;
        }, 0);
    }
};

const checks = [
    leafs({value: 1}) === 1,
    leafs({value: 1, children: [{value: 2}]}) === 1,
    leafs({value: 1, children: [{value: 2}, {value: 3}]}) === 2,
    leafs({value: 1, children: [{value: 2, children: [{value: 4}]}, {value: 3}]}) === 2,
    leafs(graph) === 269
];

console.group('Task 02');
console.log(checks);
console.log(checks.every(i => i) ? 'Everything done! ;)' : 'Something goes wrong :\'(');
console.groupEnd();

module.exports = {checks};
