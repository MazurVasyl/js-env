let canvas = document.getElementById("canvas");

canvas.width = 1000;
canvas.height = 500;

const ctx = canvas.getContext('2d');

// go to (0, 250),  which will be begining for coordinate system to build sin(x) and cos(x)
const x0 = 0;
const y0 = canvas.height / 2;

// scale
const xN = 2;
const yN = 200;

ctx.beginPath();
ctx.moveTo(x0, y0);

ctx.strokeStyle = 'green';

// sin(x) from 0 tо 360
for (let i = 1; i <= 360; i++) {
    let x = i * xN;
    let y = yN * Math.sin(i / 180 * Math.PI);
    ctx.lineTo(x0 + x, y0 - y);
}
ctx.stroke()

// cos(x) from 0 tо 360
ctx.beginPath();
ctx.strokeStyle = 'blue';
ctx.moveTo(x0,y0 - yN * Math.cos(0));

for (let i = 1; i <= 360; i++) {
    let x = i * xN;
    let y = yN * Math.cos(i / 180 * Math.PI);
    ctx.lineTo(x0 + x, y0 - y);
}
ctx.stroke();

// generate N random points within the given limits
const N = 100;
let arr = [];

const createRandomPoint = () => {
    let random;

    // we could use Math.random(), but then points will cover cos(x)-line too
    let randomNum = () => {
        random = Math.random();
        if (random === 0) {
            console.log(random);
            randomNum();
        }
        return random;
    }

    let x1 = randomNum() * 180 + 45;         // 225 - 45 = 180  --  from пі/4 to 5*пі/4, then  *2   = (90 -- 450)
    let y1 = randomNum() * (Math.sin(x1 / 180 * Math.PI) - Math.cos(x1 / 180 * Math.PI)) + Math.cos(x1 / 180 * Math.PI);

    return [x0 + x1*xN, y0 - yN * y1];
};

for (let i = 0; i < N; i++) {
    arr[i] = createRandomPoint();
}

// sort the array of coordinates of points, find the distance between them and the total length

let linesLengthSum = 0;             // the total length
let line = 0;                       // the distance between points

for (let i = 0; i < N; i++) {

    ctx.beginPath();
    ctx.strokeStyle = 'red';
    ctx.moveTo(arr[i][0],arr[i][1]);

    for (let j = i+1; j < N; j++) {

        ctx.lineTo(arr[j][0],arr[j][1]);
        ctx.moveTo(arr[i][0],arr[i][1]);

        line = Math.sqrt(Math.pow(arr[i][0] - arr[j][0],2) + Math.pow(arr[i][1] - arr[j][1],2));

        linesLengthSum += line;
    }
    ctx.stroke();
}

console.log('the total length');
console.log(linesLengthSum);


