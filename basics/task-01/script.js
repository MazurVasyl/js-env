const N = 4;

let arr = [];
let newArr = [];

let mainSum = 0;                //  the sum of the elements above the main diagonal
let minorSum = 0;               //  the sum of the elements under the minor diagonal

for(let i = 0; i < N; i++){
    arr[i] = [];
    newArr[i] = [];

    for(let j = 0; j < N; j++){
        arr[i][j] = Math.floor(Math.random() * 101);
        newArr[i][j] = arr[i][j];

        if (j > i) {
            mainSum += newArr[i][j];
        }
    }
}

console.log(newArr);
console.log(mainSum);

console.log('------------');

/* change the rows of the matrix - the minor diagonal will be the main one
     and, accordingly, the elements that were under the minor diagonal will become
     over the new main diagonal */

arr.reverse();
console.log(arr);

for(let i = 0; i < N; i++){

    for(let j = 0; j < N; j++){

        if (j > i) {
            minorSum += arr[i][j];
        }
    }
}

console.log(minorSum);

console.log('------------');

let absolute = Math.abs(mainSum - minorSum);        // the difference between the sums in absolute value
console.log(absolute);