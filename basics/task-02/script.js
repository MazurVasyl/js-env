let canv = document.getElementById('canvas');

const canvWidth = 500;
const canvHeight = 500;

canv.width = canvWidth;
canv.height = canvHeight;

const ctx = canv.getContext('2d');

const N = 4;

// create random coordinates and record them to the array

const createRandomDot = () => {
    const x1 = Math.random() * canvWidth, y1 = Math.random() * canvHeight;

    return [x1, y1];
};

let arr = [];

for (let i = 0; i < N; i++) {
    arr[i] = createRandomDot();
    console.log(arr[i]);
}

// connect coordinates with lines, determine avarege length between points

let linesLengthSum = 0;             // sum of all lines
let line = 0;                       // line between two points
let count = 0;                      // counter of lines
let avrLine = 0;                    // avarege distance
let arrLines = [];                  // the array, where we record all distance between points

for (let i = 0; i < N; i++) {

    ctx.beginPath();
    ctx.strokeStyle = 'red';
    ctx.moveTo(arr[i][0],arr[i][1]);

    for (let j = i+1; j < N; j++) {
        count++;

        ctx.lineTo(arr[j][0],arr[j][1]);
        ctx.moveTo(arr[i][0],arr[i][1]);

        line = Math.sqrt(Math.pow(arr[i][0] - arr[j][0],2) + Math.pow(arr[i][1] - arr[j][1],2));
        arrLines[count-1] = line;

        linesLengthSum += line;
    }

    ctx.stroke();
}

console.log('array of all lines:')
console.log(arrLines);

avrLine = linesLengthSum / count;
console.log('average line:')
console.log(avrLine);

let closestLine = arrLines[0];                      // suppose that the first element of the array of lengths
                                                    // is the closest to average length

let diff = Math.abs(avrLine - arrLines[0]);       // the difference between the first element and the average length
let newDiff = 0;

for (let i = 1; i < count; i++) {

    newDiff = Math.abs(avrLine - arrLines[i]);

    if (newDiff < diff) {
        diff = newDiff;
        closestLine = arrLines[i];
    }
}

console.log('closest line:');
console.log(closestLine);