const sorting = obj => {

    return  Object.entries(obj).sort((a, b) => (a[1] === b[1])
        ? Math.sign(a[0].codePointAt(0) - b[0].codePointAt(0))
        : Math.sign(b[1] - a[1])
    ).reduce((total, current) => total + current[0], '');
}

const allLettersArray = obj => {

    if (!obj.children) {
        return {[obj.letter]: 1};
    }
    else{
        return obj.children.reduce((allLetters, curLetter) => {

            curLetter = allLettersArray(curLetter);

             for (let key in curLetter) {
                 (Object.keys(allLetters).find(value => value === key))
                     ? allLetters[key] += curLetter[key]
                     : allLetters[key] = curLetter[key]
             }

             return allLetters;
        }, {[obj.letter]: 1});
    }
}

const task = graph => {
    return sorting(allLettersArray(graph));
};
console.time('graph')
console.log(task(graph));
console.timeEnd('graph')