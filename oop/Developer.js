export default class Developer {
    constructor(firstName, lastName, assignedTasks) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.assignedTasks = assignedTasks;
    }

    set assign(task) {
        if (!this.assignedTasks.find(value => value.name === task.name)) {
            this.assignedTasks.push(task);
        }
        task.assign = this;
    }

    get fullName() {
        return `${this.firstName} ${this.lastName}`;
    }
}
