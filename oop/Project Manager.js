export default class ProjectManager {
    constructor(firstName, lastName, assignedReleases) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.assignedReleases = assignedReleases;
    }

    set assign(release) {
        if (!this.assignedReleases.find(value => value.name === release.name)) {
            this.assignedReleases.push(release);
        }
        release.assign = this;
    }
}
