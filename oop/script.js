import Developer from "./Developer.js";
import ProjectManager from "./Project Manager.js";
import Task from "./Task.js";
import Release from "./Release.js";

const counter = (x = 0) => () => ++x;

Task.f = counter();

let t1 = new Task('t1', 'group 1', 'open', 0, 0, '');
let t2 = new Task('t2', 'group 1', 'open', 0, 0, '');
let t3 = new Task('t3', 'group 1', 'open', 0, 0, '');
let t4 = new Task('t4', 'group 2', 'open', 0, 0, '');
let t5 = new Task('t5', 'group 1', 'open', 0, 0, '');
let t6 = new Task('t6', 'group 2', 'open', 0, 0, '');
let t7 = new Task('t7', 'group 2', 'open', 0, 0, '');
let t8 = new Task('t8', 'group 2', 'open', 0, 0, '');

let dev1 = new Developer('Ivan', 'Ivanov', []);
let dev2 = new Developer('Petro', 'Petrov', []);
let dev3 = new Developer('Pavlo', 'Pavlov', []);
let dev4 = new Developer('Igor', 'Igorov', []);
let dev5 = new Developer('Maxym', 'Maxymov', []);

let pm1 = new ProjectManager('Oleg', 'Olegov', []);
let pm2 = new ProjectManager('Vova', 'Vovov', []);

let rl1 = new Release('rl1', 'group 1', new Date(), []);
let rl2 = new Release('rl2', 'group 2', new Date(),[]);

dev1.assign = t1;
dev2.assign = t2;
dev3.assign = t3;
dev4.assign = t4;
dev1.assign = t5;
t6.assign = dev1;
t7.assign = dev5;
t8.assign = dev2;

t1.Status = 'in dev';
t2.Status = 'in dev';
t3.Status = 'in dev';
t4.Status = 'in dev';
t5.Status = 'in dev';
t6.Status = 'in dev';
t7.Status = 'in dev';
t8.Status = 'in dev';

t1.Estimate = 600;
t2.Estimate = 720;
t3.Estimate = 840;
t4.Estimate = 960;
t5.Estimate = 3960;
t6.Estimate = 940;
t7.Estimate = 960;
t8.Estimate = 1960;

t1.LoggedWork = 80;
t2.LoggedWork = 30;
t3.LoggedWork = 50;
t4.LoggedWork = 60;
t5.LoggedWork = 30;
t6.LoggedWork = 70;
t7.LoggedWork = 30;
t8.LoggedWork = 10;

rl1.assign = pm1;
rl2.assign = pm2;

rl1.addTask = t1;
rl1.addTask = t2;
rl1.addTask = t3;
rl1.addTask = t5;
rl2.addTask = t4;
rl2.addTask = t6;
rl2.addTask = t7;
rl2.addTask = t8;

rl1.newReleaseDate = new Date(2021, 1, 21, 18, 30);
rl2.newReleaseDate = new Date(2020, 11, 8, 18, 30);

Release.control(rl1);
Release.control(rl2);
