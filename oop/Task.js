export default class Task {
    constructor(name, description, status, estimate, loggedWork, assignee) {
        this.ID = Task.f();
        this.name = name;
        this.description = description;
        this.status = status;
        this.estimate = estimate;
        this.loggedWork = loggedWork;
        this.assignee = assignee;
    }

    set assign(dev) {
        if (!dev.assignedTasks.find(value => value.name === this.name)) {
            dev.assignedTasks.push(this);
        }
        this.assignee = dev;
    }

    set Status(s) {
        this.status = s;
    }

    set Estimate(t) {
        this.estimate = t;
    }

    set LoggedWork(t) {
        this.loggedWork += t;
    }

    get restOfTime() {
        return 1.3 * this.estimate - this.loggedWork;
    }
}


