export default class DataTable {
    #parent;
    #data;
    static lines = ['5', '10', '25', '50', '100', '250'];

    constructor(parent) {
        this.#parent = parent;
    }

    set data(data) {
        this.#data = data;
    }

    static checkingToAllKeys = (data) => {

        const maxLength = Math.max(...data.map(el => Object.keys(el).length));
        const firstMaxLength = data.find(el => Object.keys(el).length === maxLength);

        Object.keys(firstMaxLength).forEach(key => {
            data.reduce((total, current) => {

                if (Object.keys(current).find(element => element === key)) {
                    const value = current[key];
                    delete current[key];
                    current[key] = value;
                }
                else {
                    current[key] = '-';
                }

                total.push(current);

                return total;
            }, []);
        })
        return data;
    }

    #toSort = (num, parent, data, key, keyNumber, selectedNum, direction) => {
        (direction === 'down')
            ? data.sort((a, b) =>
                (typeof a[key] !== 'string') ? b[key] - a[key] : b[key].localeCompare(a[key])
            )
            : data.sort((a, b) =>
                (typeof a[key] !== 'string') ? a[key] - b[key] : a[key].localeCompare(b[key])
            );

        this.#cleanArticle(parent);
        this.#buildArticle(num, parent, data, selectedNum, 0, keyNumber, direction);
        this.#cleanPageNumbers(parent);
        this.#buildPageNumbers(num, parent, data, selectedNum, keyNumber, direction);
    }

    #hiddenElements = (parent) => {
        if (parent.querySelectorAll('span')) {
            parent.querySelectorAll('span').forEach(el => {
                el.remove();
            })
        }

        const pagesList = parent.querySelectorAll(`.page-number-item`);
        pagesList.forEach((element, index) => {
            if (element.classList.contains('hidden-number') && !pagesList[index-1].classList.contains('hidden-number')) {
                const hiddenElements = document.createElement('span');
                hiddenElements.textContent = '...';
                parent.insertBefore(hiddenElements, element);
            }
        })
    }

    #displayInline = (parent, start, end) => {
        const numbers = parent.querySelectorAll(`.page-number-item`);

        if (start < 0) {
            start = 0;
        }
        if (end >= numbers.length) {
            end = numbers.length-1;
        }

        numbers.forEach(element => element.classList.add('hidden-number'));
        numbers[0].classList.remove('hidden-number');
        numbers[numbers.length-1].classList.remove('hidden-number');
        for (let j = start; j <= end; j++) {
            numbers[j].classList.remove('hidden-number');
        }
    }

    #cleanArticle = (parent) => parent.querySelector(`article`).remove();

    #cleanPageNumbers = (parent) => parent.querySelector(`.page-number`).remove();

    #buildSelect = (num, parent, data) => {
        const select = document.createElement('select');
        parent.children[parent.children.length - 1].append(select);
        select.onmouseover = () => select.style.cursor = 'pointer';

        DataTable.lines.forEach(value => {
            const option = document.createElement('option');
            select.append(option);
            option.textContent = value;
            option.value = value;
        });

        select.addEventListener('change', (event) => {
            num = Math.ceil(data.length / Number(event.target.value));

            this.#toSort(num, parent, data, Object.keys(data[0])[0], 0, Number(event.target.value), 'up');
        });

        select.children[1].setAttribute('selected', '');
    }

    #buildPageNumbers = (num, parent, data, selectedNum, numberOfSortedField, direction) => {
        const pageNumber = document.createElement('ul');
        parent.children[parent.children.length - 1].append(pageNumber);
        pageNumber.className = `page-number`;

        for (let i = 0; i < num; i++) {
            const li = document.createElement('li');
            pageNumber.append(li);
            li.textContent = `${i + 1}`;
            li.className = `page-number-item`;
            li.onclick = () => {

                this.#cleanArticle(parent);
                this.#buildArticle(num, parent, data, selectedNum, i, numberOfSortedField, direction);

                pageNumber.querySelector(`.page-number-item.active-page-number`).classList.remove(`active-page-number`);
                pageNumber.querySelectorAll(`.page-number-item`)[i].classList.add(`active-page-number`);

                this.#displayInline(pageNumber, i-1, i+1);
                this.#hiddenElements(pageNumber);
            }
        }

        pageNumber.querySelector(`.page-number-item`).classList.add(`active-page-number`);
        this.#displayInline(pageNumber, 0, 1);
        this.#hiddenElements(pageNumber);
    }

    #buildArticle = (num, parent, data, selectedNum, numberOfPage, numberOfSortedField, direction) => {

        const article = document.createElement('article');
        parent.prepend(article);

        const table = document.createElement('table');
        table.className = 'table';
        article.append(table);

        const thead = document.createElement('thead');
        table.append(thead);

        const tr = document.createElement('tr');
        thead.append(tr);

        Object.keys(data[0]).forEach((key, index) => {
            const th = document.createElement('th');
            th.textContent = `${key}`;
            tr.append(th);

            const spanDown = document.createElement('span');
            th.append(spanDown);
            spanDown.className = 'glyphicon glyphicon-triangle-bottom';
            spanDown.onclick = () => this.#toSort(num, parent, data, key, index, selectedNum, 'down');

            const spanUp = document.createElement('span');
            th.append(spanUp);
            spanUp.className = 'glyphicon glyphicon-triangle-top';
            spanUp.onclick = () => this.#toSort(num, parent, data, key, index, selectedNum, 'up');
        });

        const tbody = document.createElement('tbody');
        table.append(tbody);

        let end = numberOfPage * Number(selectedNum) + Number(selectedNum);
        if (end >= data.length) {
            end = data.length;
        }

        for (let j = numberOfPage * Number(selectedNum); j < end; j++) {
            const tr = document.createElement('tr');
            tbody.append(tr);

            Object.keys(data[0]).forEach(key => {
                const value = Object.entries(data[j]).find(el => el[0] === key);

                const td = document.createElement('td');
                td.textContent = `${value[1]}`;
                tr.append(td);
            });
        }

        parent.querySelectorAll('.sorted').forEach(el => {
            el.classList.remove('sorted');
        });

        (direction === 'down')
            ? parent.querySelectorAll('.glyphicon.glyphicon-triangle-bottom')[numberOfSortedField].classList.add('sorted')
            : parent.querySelectorAll('.glyphicon.glyphicon-triangle-top')[numberOfSortedField].classList.add('sorted');

    }

    build() {
        let numberOfPages = Math.ceil(this.#data.length / Number(DataTable.lines[1]));
        const fixedData = DataTable.checkingToAllKeys(this.#data);

        const section = document.createElement('section');
        this.#parent.append(section);

        const footer = document.createElement('footer');
        section.append(footer);
        footer.className = 'footer';

        this.#buildSelect(numberOfPages, section, fixedData);
        this.#buildPageNumbers(numberOfPages, section, fixedData, Number(DataTable.lines[1]), 0, 'up');
        this.#buildArticle(numberOfPages, section, fixedData, Number(DataTable.lines[1]), 0, 0, 'up');
    }
}
